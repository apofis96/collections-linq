﻿namespace ConsoleApp.Models
{
    public enum TaskState
    {
        Created,
        Started,
        Finished,
        Canceled
    }
}
