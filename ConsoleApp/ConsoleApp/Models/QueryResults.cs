﻿using System.Collections.Generic;

namespace ConsoleApp.Models
{
    public class FinishedTasks
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
    public class TeamMembers
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<User> Members { get; set; }
    }
    public class UserTasks
    {
        public User User { get; set; }
        public List<ProjectTask> Tasks { get; set; }
    }
    public class UserStatistic
    {
        public User User { get; set; }
        public Project LastProject { get; set; }
        public int LastProjectTasksCount { get; set; }
        public int NotFinishedTasksCount { get; set; }
        public ProjectTask LongestTask { get; set; }
    }
    public class ProjectStatistic
    {
        public Project Project { get; set; }
        public ProjectTask LongestDescriptionTask { get; set; }
        public ProjectTask ShortestNameTask { get; set; }
        public int MembersTeamCount { get; set; }
    }


}
