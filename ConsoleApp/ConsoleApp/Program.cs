﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;
using ConsoleApp.Models;

namespace ConsoleApp
{
    class Program
    {
        static readonly HttpClient client = new HttpClient();
        static string host = "https://bsa20.azurewebsites.net/api/";
        static JsonSerializerOptions options = new JsonSerializerOptions
        {
            PropertyNameCaseInsensitive = true,
            WriteIndented = true,
        };
        static List<Project> projects = new List<Project>();
        static void Main(string[] args)
        {
            client.BaseAddress = new Uri(host);
            Run().GetAwaiter().GetResult();

            /*projects = GetDataFromServer().Result;
            var t1 = GetTasksCountByUser(6);
            var t2 = GetTasksByUserList(2);
            var t3 = GetFinishedTasksList(2);;
            var t4 = GetTeamMembersList();
            var t5 = GetUserTasksList();
            var t6 = GetUserStatistics(3);
            var t7 = GetProjectStatisticsList();*/
        }
        static async Task<List<Project>> GetDataFromServer()
        {
            var projects = JsonSerializer.Deserialize<List<Project>>(await client.GetStringAsync("Projects"), options);
            var tasks = JsonSerializer.Deserialize<List<ProjectTask>>(await client.GetStringAsync("Tasks"), options);
            var users = JsonSerializer.Deserialize<List<User>>(await client.GetStringAsync("Users"), options);
            var teams = JsonSerializer.Deserialize<List<Team>>(await client.GetStringAsync("Teams"), options);

            tasks = tasks.Join(users,
                t => t.PerformerId, u => u.Id, (t, u) =>
                {
                    t.Performer = u;
                    return t;
                }).ToList();

            projects = projects.GroupJoin(tasks,
                p => p.Id, ts => ts.ProjectId, (p, ts) =>
                {
                    p.Tasks = ts.ToList(); ;
                    return p;
                }).ToList();

            projects = projects.Join(users,
                p => p.AuthorId, u => u.Id, (p, u) =>
                {
                    p.Author = u;
                    return p;
                }).ToList();

            projects = projects.Join(teams,
                p => p.TeamId, t => t.Id, (p, t) =>
                {
                    p.Team = t;
                    return p;
                }).ToList();
            return projects;
        }
        static Dictionary<Project, int> GetTasksCountByUser(int userId)
        {
            return projects.Where(p => p.AuthorId == userId)
                .ToDictionary(k => k, k => k.Tasks.Where(t => t.PerformerId == userId).Count());
        }
        static List<ProjectTask> GetTasksByUserList(int userId)
        {
            return projects.GetTasks().Where(t => t.PerformerId == userId && t.Name.Length < 45).ToList();
        }
        static List<FinishedTasks> GetFinishedTasksList (int userId)
        {
            return projects.GetTasks()
                .Where(t => t.PerformerId == userId && t.State == TaskState.Finished && t.FinishedAt.Year == 2020)
                .Select(t => new FinishedTasks 
                { 
                    Id = t.Id, 
                    Name = t.Name 
                }).ToList();
        }
        static List<TeamMembers> GetTeamMembersList()
        {
            return projects.Select(p => p.Team).Distinct().Join(projects.GetUsers()
                .Where(u => (DateTime.Today.Year - u.Birthday.Year) > 10)
                .OrderByDescending(u => u.RegisteredAt).GroupBy(u => u.TeamId),
                t => t.Id, u => u.Key, (t, u) => new TeamMembers
                {
                    Id = t.Id,
                    Name  = t.Name,
                    Members = u.ToList()
                }).ToList();
        }
        static List<UserTasks> GetUserTasksList()
        {
            return projects.GetUsers().OrderBy(u => u.FirstName)
                .GroupJoin(projects.GetTasks().OrderByDescending(t => t.Name.Length), 
                u => u.Id, t => t.PerformerId, (u, t) => new UserTasks
                {
                    User = u,
                    Tasks = t.ToList()
                }).ToList();
        }
        static UserStatistic GetUserStatistics(int userId)
        {
            return projects.GetUsers().Where(u => u.Id == userId).GroupJoin(
                projects.GetTasks().OrderByDescending(t => t.FinishedAt - t.CreatedAt),
                u => u.Id, t => t.PerformerId, (u, t) => new
                {
                    User = u,
                    Tasks = t
                }).GroupJoin(projects.OrderByDescending(p => p.CreatedAt), 
                z => z.User.Id, p => p.AuthorId, (z, p) => new UserStatistic
                {
                    User = z.User,
                    LastProject = p.FirstOrDefault(),
                    LastProjectTasksCount = p.FirstOrDefault().Tasks.Count,
                    NotFinishedTasksCount = z.Tasks.Where(t => t.State != TaskState.Finished).Count(),
                    LongestTask = z.Tasks.FirstOrDefault()
                }).FirstOrDefault();
        }
        static List<ProjectStatistic> GetProjectStatisticsList()
        {
            return projects.Select(p => new ProjectStatistic
            {
                Project = p,
                LongestDescriptionTask = p.Tasks.OrderByDescending(t => t.Description.Length).FirstOrDefault(),
                ShortestNameTask = p.Tasks.OrderBy(t => t.Name.Length).FirstOrDefault(),
                MembersTeamCount = projects.GetUsers().Where(u => u.TeamId == p.TeamId && (p.Description.Length > 20 || p.Tasks.Count < 3)).Count()
            }).ToList();
        }
        static async Task Run()
        {
            projects = await GetDataFromServer();

            string menuItems = @"1 : Task_1.GetTasksCountByUser
2 : Task_2.GetTasksByUserList
3 : Task_3.GetFinishedTasksList
4 : Task_4.GetTeamMembersList
5 : Task_5.GetUserTasksList
6 : Task_6.GetUserStatistics
7 : Task_7.GetProjectStatisticsList
0 : Exit.";
            string warning = "Sequence output is limited to three objects!";
            string pause = "Press any key to continue";
            Console.WriteLine(menuItems);
            bool loop = true;
            while (loop)
            {
                Console.Write("Main menu input: ");
                char menu = Console.ReadKey().KeyChar;
                Console.WriteLine("\n================================");
                switch (menu)
                {
                    case '1':
                        Console.WriteLine(warning);
                        var res = GetTasksCountByUser(UserIdInput());
                        int i = 0;
                        foreach (KeyValuePair<Project, int> kvp in res)
                        {
                            Console.WriteLine("Key = " + JsonSerializer.Serialize(kvp.Key, options));
                            Console.WriteLine(" Value = "+ kvp.Value);
                            if (++i == 3) break;
                        }
                        break;
                    case '2':
                        Console.WriteLine(warning);
                        Console.WriteLine(JsonSerializer.Serialize(GetFinishedTasksList(UserIdInput()).Take(3), options));
                        break;
                    case '3':
                        Console.WriteLine(warning);
                        Console.WriteLine(JsonSerializer.Serialize(GetFinishedTasksList(UserIdInput()).Take(3), options));
                        break;
                    case '4':
                        Console.WriteLine(warning);
                        Console.WriteLine(pause);
                        Console.ReadKey();
                        Console.WriteLine(JsonSerializer.Serialize(GetTeamMembersList().Take(3), options));
                        break;
                    case '5':
                        Console.WriteLine(warning);
                        Console.WriteLine(pause);
                        Console.ReadKey();
                        Console.WriteLine(JsonSerializer.Serialize(GetUserTasksList().Take(3), options));
                        break;
                    case '6':
                        Console.WriteLine(JsonSerializer.Serialize(GetUserStatistics(UserIdInput()), options));
                        break;
                    case '7':
                        Console.WriteLine(warning);
                        Console.WriteLine(pause);
                        Console.ReadKey();
                        Console.WriteLine(JsonSerializer.Serialize(GetProjectStatisticsList().Take(3), options));
                        break;
                    case '0':
                        loop = false;
                        break;
                    default:
                        Console.WriteLine(menuItems);
                        break;
                }
            }
        }
        static int UserIdInput()
        {
            int input;
            do
            {
                Console.Write("Input UserID: ");
                input = int.TryParse(Console.ReadLine(), out input) ? input : -1;
                if (input < 0)
                {
                    Console.WriteLine("Wrong input");
                }
                else
                    return input;
            }
            while (true);
        }
    }
    public static class LINQExtension
    {
        public static IEnumerable<User> GetUsers(this IEnumerable<Project> source)
        {
            return source.Select(p => p.Author).Union(source.SelectMany(p => p.Tasks).Select(t => t.Performer)).Distinct();
        }
        public static IEnumerable<ProjectTask> GetTasks(this IEnumerable<Project> source)
        {
            return source.SelectMany(p => p.Tasks);
        }
    }
}
